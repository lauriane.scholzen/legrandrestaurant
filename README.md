# LeGrandRestaurant

## Test de recette scope Chiffre d'Affaires

N° | Spécifications | Test effectué | Résultat attendu du test | Test valide ou non
--- | --- | --- | --- |---
1 | ÉTANT DONNÉ un nouveau serveur, QUAND on récupére son chiffre d'affaires, ALORS celui-ci est à 0 | Création d'un nouveau serveur avec un chiffre d'affaires à 0,0 | Le serveur est créé et son CA est de 0,0 | OK
--- | --- | --- | --- |---
2 | ÉTANT DONNÉ un nouveau serveur, QUAND il prend une commande, ALORS son chiffre d'affaires est le montant de celle-ci | Création d'un nouveau serveur, ce serveur prend une commande d'un certain montant | Le serveur est créé et son CA est de 0,0, après la prise de commande, le CA du serveur devient celui du montant de la commande | OK
--- | --- | --- | --- |---
3 | ÉTANT DONNÉ un serveur ayant déjà pris une commande, QUAND il prend une nouvelle commande, ALORS son chiffre d'affaires est la somme des deux commandes | Création d'un nouveau serveur avec un CA de 0,0, ce serveur prend une commande d'un certain montant, il prend une nouvelle commande | Le serveur est créé et son CA est de 0,0, après la prise de commande, le CA du serveur devient celui du montant de la commande. Quand le serveur prend une nouvelle commande, son CA devient la somme des 2 commandes | OK
--- | --- | --- | --- |---
4 | ÉTANT DONNÉ un restaurant ayant X serveurs, QUAND tous les serveurs prennent une commande d'un montant Y, ALORS le chiffre d'affaires de la franchise est X * Y | Création d'une franchise ayant un restaurant avec un nombre de variable de serveurs, chaque serveur prend une commande d'un certain montant | La franchise est créée avec un restaurant ayant plusieurs serveurs, chaque serveur prend une commande d'un montant donné, le CA de la franchise doit être égal au nombre de serveurs multiplié par le montant de la commande | OK
--- | --- | --- | --- |---
5 | ÉTANT DONNÉ une franchise ayant X restaurants de Y serveurs chacun, QUAND tous les serveurs prennent une commande d'un montant Z, ALORS le chiffre d'affaires de la franchise est X * Y * Z | Création d'une franchise ayant un nombre variable de restaurants ayant eux-même un nombre variable de serveurs, chaque serveur prend une commande d'un certain montant | La franchise est créée avec plusieurs restaurants ayant plusieurs serveurs, chaque serveur prend une commande d'un montant donné, le CA de la franchise doit être égal au nombre de restaurants multiplié par le nombre de serveurs multiplié par le montant de la commande | OK

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/lauriane.scholzen/legrandrestaurant.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://gitlab.com/lauriane.scholzen/legrandrestaurant/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:d17e03df337925b9c0f25b074a58fd2a?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
