package com.legrandrestaurantmaven;

import java.util.ArrayList;
import java.util.Map;

public class Bar {
    
    private ArrayList<Commande> listeCommandes;
    
    public Bar() {
        this.listeCommandes = new ArrayList<Commande>();
    }
    
    public void nouvelleCommande(int numCommande) {
        Commande c = new Commande();
        c.ajouterCommandeBoissons("Coca", 2.5);
        this.listeCommandes.add(c);
    }
    
    public void attribuerCommande(Barman b) {
        if(this.listeCommandes.size() > 0) {
            for(Commande c : this.listeCommandes) {
                b.getListeCommandes().add(c);
            }
        }
    }
    
    public void retirerCommandeTerminee(Commande c) {
        int index = 0;
        for(Commande c1 : this.listeCommandes) {
            if(c == c1) {
                index = this.listeCommandes.indexOf(c1);
            }
        }
        this.listeCommandes.remove(index);
    }

    public ArrayList<Commande> getListeCommandes() {
        return listeCommandes;
    }

    public void setListeCommandes(ArrayList<Commande> listeCommandes) {
        this.listeCommandes = listeCommandes;
    }
        
} // end of class
