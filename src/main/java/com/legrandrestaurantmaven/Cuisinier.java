package com.legrandrestaurantmaven;

import java.util.ArrayList;

public class Cuisinier extends Employe {
    
    private ArrayList<Commande> listeCommandes;
    
     public Cuisinier(String nomEmploye, Restaurant r) {
        super(nomEmploye, "Cuisinier", r);
        this.listeCommandes = new ArrayList<Commande>();
    } 
     
    public ArrayList<Commande> getListeCommandes() {
        return listeCommandes;
    }

    public void setListeCommandes(ArrayList<Commande> listeCommandes) {
        this.listeCommandes = listeCommandes;
    }
     
} // end of class
