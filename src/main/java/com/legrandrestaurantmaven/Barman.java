package com.legrandrestaurantmaven;

import java.util.ArrayList;

public class Barman extends Employe {
    
    private ArrayList<Commande> listeCommandes;
    
    public Barman(String nomEmploye, Restaurant r) {
        super(nomEmploye, "Barman", r);
        this.listeCommandes = new ArrayList<Commande>();
    }
    
    public Commande terminerCommande(Commande c) {
        Commande commandeARetirer = null;
        int index = 0;
        for(Commande c1 : this.listeCommandes) {
            if(c == c1) {
                commandeARetirer = c;
                index = this.listeCommandes.indexOf(c1);
            }
        }
        this.listeCommandes.remove(index);
        return commandeARetirer;
    }
    
    public ArrayList<Commande> getListeCommandes() {
        return listeCommandes;
    }

    public void setListeCommandes(ArrayList<Commande> listeCommandes) {
        this.listeCommandes = listeCommandes;
    }
    
} // end of class
